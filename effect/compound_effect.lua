-- compound_effect.lua
-- A two-pass effect, blending the results of 2 buffers

compound_effect = {}

local openGL = require("opengl")
local ffi = require("ffi")
local sf = require("util.shaderfunctions")
local fbf = require("util.fbofunctions")

local glIntv     = ffi.typeof('GLint[?]')
local glUintv    = ffi.typeof('GLuint[?]')
local glFloatv   = ffi.typeof('GLfloat[?]')

local blur_effect = require("effect.twopassblur_effect")
local edge_effect = require("effect.onepassblur_effect")

local vao = 0
local prog = 0
local vbos = {}

local ds = 1

local basic_vert = [[
#version 410 core

in vec4 vPosition;
in vec4 vColor;
out vec3 vfTexCoord;

void main()
{
    vfTexCoord = vec3(.5*(vColor.xy+1.),0.);
    gl_Position = vec4(vPosition.xy, 0., 1.);
}
]]

local basic_frag = [[
#version 330

in vec3 vfTexCoord;
out vec4 fragColor;
uniform sampler2D tex;
uniform sampler2D texEdge;

void main()
{
    vec4 tval = texture(tex, vfTexCoord.xy);
    vec4 eval = texture(texEdge, vfTexCoord.xy);
    vec3 val = (tval.rgb + 2.*eval.rgb);
    fragColor = vec4(val,1.);
}
]]


local function init_quad_attributes()
    local verts = glFloatv(4*2, {
        -1,-1,
        1,-1,
        1,1,
        -1,1,
        })

    local vpos_loc = gl.GetAttribLocation(prog, "vPosition")
    local vcol_loc = gl.GetAttribLocation(prog, "vColor")

    local vvbo = glIntv(0)
    gl.GenBuffers(1, vvbo)
    gl.BindBuffer(GL.ARRAY_BUFFER, vvbo[0])
    gl.BufferData(GL.ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.STATIC_DRAW)
    gl.VertexAttribPointer(vpos_loc, 2, GL.FLOAT, GL.FALSE, 0, nil)
    table.insert(vbos, vvbo)

    local cvbo = glIntv(0)
    gl.GenBuffers(1, cvbo)
    gl.BindBuffer(GL.ARRAY_BUFFER, cvbo[0])
    gl.BufferData(GL.ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.DYNAMIC_DRAW)
    sf.CHECK_GL_ERROR()
    gl.VertexAttribPointer(vcol_loc, 2, GL.FLOAT, GL.FALSE, 0, nil)
    table.insert(vbos, cvbo)

    sf.CHECK_GL_ERROR()
    gl.EnableVertexAttribArray(vpos_loc)
    gl.EnableVertexAttribArray(vcol_loc)

    local quads = glUintv(3*2, {
        1,0,2,
        2,0,3,
    })
    local qvbo = glIntv(0)
    gl.GenBuffers(1, qvbo)
    gl.BindBuffer(GL.ELEMENT_ARRAY_BUFFER, qvbo[0])
    gl.BufferData(GL.ELEMENT_ARRAY_BUFFER, ffi.sizeof(quads), quads, GL.STATIC_DRAW)
    table.insert(vbos, qvbo)

    sf.CHECK_GL_ERROR()
end

local function draw_fullscreen_quad()
    gl.BindVertexArray(vao)
    gl.DrawElements(GL.TRIANGLES, 3*2, GL.UNSIGNED_INT, nil)
    gl.BindVertexArray(0)
end

--------------------------------------------
-- Exports

function compound_effect.initGL()
    blur_effect.initGL()
    edge_effect.initGL()

    vbos = {}
    texs = {}
    local vaoId = ffi.new("int[1]")
    gl.GenVertexArrays(1, vaoId)
    vao = vaoId[0]
    gl.BindVertexArray(vao)

    prog = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = basic_frag,
        })

    init_quad_attributes()
    gl.BindVertexArray(0)
end

function compound_effect.exitGL()
    blur_effect.exitGL()
    edge_effect.exitGL()

    for k,v in pairs(vbos) do
        gl.DeleteBuffers(1,v)
    end
    vbos = nil
    gl.DeleteProgram(prog)

    local vaoId = ffi.new("GLuint[1]", vao)
    gl.DeleteVertexArrays(1, vaoId)

    fbf.deallocate_fbo(compound_effect.fbo)
end

function compound_effect.present_texture(texId, resx, resy)
    --blur_effect.present_texture(texId, resx, resy)

    -- Draw edge effect into its own buffer
    edge_effect.bind_fbo()
    gl.Viewport(0,0, resx/ds, resy/ds)
    edge_effect.present_texture(texId, resx/ds, resy/ds)
    edge_effect.unbind_fbo()

    gl.Viewport(0,0, resx, resy)

    gl.UseProgram(prog)

    gl.ActiveTexture(GL.TEXTURE0)
    gl.BindTexture(GL.TEXTURE_2D, texId)
    local tx_loc = gl.GetUniformLocation(prog, "tex")
    gl.Uniform1i(tx_loc, 0)

    gl.ActiveTexture(GL.TEXTURE1)
    gl.BindTexture(GL.TEXTURE_2D, edge_effect.fbo.tex)
    local txe_loc = gl.GetUniformLocation(prog, "texEdge")
    gl.Uniform1i(txe_loc, 1)

    local rx_loc = gl.GetUniformLocation(prog, "ResolutionX")
    gl.Uniform1i(rx_loc, resx)
    local ry_loc = gl.GetUniformLocation(prog, "ResolutionY")
    gl.Uniform1i(ry_loc, resy)

    draw_fullscreen_quad()
    sf.CHECK_GL_ERROR()

    gl.UseProgram(0)
end

function compound_effect.bind_fbo()
    if fbo then fbf.bind_fbo(compound_effect.fbo) end
end

function compound_effect.unbind_fbo()
    fbf.unbind_fbo()
end

function compound_effect.resize_fbo(w,h)
    edge_effect.resize_fbo(w/ds, h/ds)

    local e = compound_effect
    if e.fbo then fbf.deallocate_fbo(e.fbo) end
    e.fbo = fbf.allocate_fbo(w,h)
end

function compound_effect.present()
    --edge_effect.present()

    local e = compound_effect
    local f = e.fbo
    e.present_texture(f.tex, f.w, f.h)
end

return compound_effect
