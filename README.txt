Sechs

Music: CapsAdmin
Code: Jimbo with liberal helpings of snippets from Shadertoy: ThiSpawn, iq, Ippokratis
Vortices: Mark Stock

This demo was crafted around CapsAdmin's song 'she'. It started with building a DNA double helix in vertex shader for use with instancing. It probably could have been done using regular geometry and AttribDivisor, but it seemed clever at the time - right up until I ran it on Turks and it crashed with a warning message about register pressure. :/ Repurposing the DNA generation code yielded a few different looking creatures, so an evolution concept came into focus. Noticing the German pun on the word 'six' brought it all together, so I ripped off some sixth order Julia set code from Shadertoy to complete the theme.

This demo should be mostly VR ready as it does not use any cameras - all objects move in space relative to a fixed viewpoint. Porting it to VR would be a matter of refactoring to get the Bass music syncing component pulled out of main and into a module that can be called from https://bitbucket.org/jimbo00000/riftskel . All scenes were created using the Luajit GL framework and should just drop into riftskel without modification. I wish I had the time to try it...

The final explosion uses vortex advection code written by Mark Stock and was just too pretty to leave collecting dust on a drive.

Thanks to:

CapsAdmin - I can't get enough of this music! - https://soundcloud.com/capsadmin
Roberto Ierusalimschy for Lua - http://www.lua.org/
Mike Pall for Luajit - http://luajit.org/
kusma, emoon for Rocket - https://github.com/emoon/rocket
daurnimator for luajit help
lpghatguy for luajit help and Coeus - https://github.com/titan-studio/coeus
malkia for ufo - https://github.com/malkia/ufo
Khronos for OpenGL - https://www.opengl.org/
Dr. Claw for @party - http://atparty-demoscene.net/
iq - http://www.iquilezles.org/
BeautyPi - http://www.beautypi.com/
all @party attendees
