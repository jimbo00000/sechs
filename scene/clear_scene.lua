-- clear_scene.lua

clear_scene = {}

local openGL = require("opengl")

function clear_scene.initGL()
end

function clear_scene.exitGL()
end

function clear_scene.render_for_one_eye(view, proj)
end

function clear_scene.timestep(absTime, dt)
end

return clear_scene
