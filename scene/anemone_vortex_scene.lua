-- anemone_vortex_scene.lua

anemone_vortex_scene = {}

local openGL = require("opengl")

local AS = require("scene.anemone2_scene")
local VS = require("scene.vortices_scene")

anemone_vortex_scene.anemscale = 2
anemone_vortex_scene.armrot_x = 0
anemone_vortex_scene.armrot_y = 0

function anemone_vortex_scene.initGL()
    AS.initGL()
    VS.initGL()
end

function anemone_vortex_scene.exitGL()
    AS.exitGL()
    VS.exitGL()
end

function anemone_vortex_scene.render_for_one_eye(view, proj)
    AS.render_for_one_eye(view, proj)
    VS.render_for_one_eye(view, proj)
end

function anemone_vortex_scene.timestep(absTime, dt)
    AS.timestep(absTime, dt)
    VS.timestep(absTime, dt)

    -- hackaround for setting child scenes' variables
    AS.anemscale = anemone_vortex_scene.anemscale
    AS.armrot_x = anemone_vortex_scene.armrot_x
    AS.armrot_y = anemone_vortex_scene.armrot_y
end

return anemone_vortex_scene
