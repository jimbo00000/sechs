-- julia6quad_scene.lua
julia6quad_scene = {}

local openGL = require("opengl")
local ffi = require("ffi")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")

local glIntv     = ffi.typeof('GLint[?]')
local glUintv    = ffi.typeof('GLuint[?]')
local glFloatv   = ffi.typeof('GLfloat[?]')

local vbos = {}
local vao = 0
local prog = 0
local g_time = 0

julia6quad_scene.tweakx = 0
julia6quad_scene.tweaky = 0

local basic_vert = [[
#version 330

in vec4 vPosition;
in vec4 vColor;

out vec3 vfColor;

uniform mat4 mvmtx;
uniform mat4 prmtx;

void main()
{
    vfColor = vColor.xyz;
    gl_Position = prmtx * mvmtx * vPosition;
}
]]

local basic_frag = [[
#version 330

in vec3 vfColor;
out vec4 fragColor;

uniform float iGlobalTime;
uniform vec2 tweak;

// https://www.shadertoy.com/view/XtXSR8
// https://www.shadertoy.com/view/XsXGz2

#define iMax 1024.

const float PI = 3.1415926535897932384626433832795;

vec2 complex_pow(vec2 z,float power)
{   
    float real = cos(power * atan(z.x / z.y) - power * PI * sign(z.y) * 0.5) * pow(z.x * z.x + z.y * z.y, power / 2.0 );
    float imag = -sin(power * atan(z.x / z.y) - power * PI * sign(z.y) * 0.5) * pow(z.x * z.x + z.y * z.y, power / 2.0 );
    return vec2(real, imag);    
}

vec3 julia(vec2 z)
{
#if 0
   // vec2 c = vec2(sin(iGlobalTime*0.5),cos(iGlobalTime*0.5));
   // vec2 c = vec2(-0.1,0.85)+sin(iGlobalTime*0.5)*0.05;
    //vec2 tweak = .005*iMouse.xy;
    float t = 2.*iGlobalTime;
    vec2 tweak = .005*200.*vec2(
        1.3 +.025*sin(t),
        1.2 +.025*cos(1.3*t)
    );
#endif
    vec2 c = tweak*vec2(-0.125,0.7252);
    float j;
    for(float i = 0.0 ; i<iMax; i++)
    {
        j = i;
        z = complex_pow(z, 6.) + c;
        float d = length(z);
        if(d > 2.0) break;
    }
    return vec3(j/64.,j/32.,j/16.);  
}

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    vec2 uv = fragCoord.xy;
    uv = 2.0*uv-1.0;
    float ang = 6.;//sin(iGlobalTime*(-0.25));
    mat2 rotation = mat2(cos(ang), sin(ang),-sin(ang), cos(ang));
    uv*=rotation * 1.2;
   // uv*=sin(iGlobalTime*0.1)*0.5+1.0;
    
    vec3 col = julia(uv);
    fragColor = vec4(col,step(.5, length(col)));
}

void main()
{
    mainImage(fragColor, .5*vfColor.xy+vec2(.5));
}
]]


local function init_cube_attributes()
    local verts = glFloatv(3*4, {
        -1,-1,0,
        1,-1,0,
        1,1,0,
        -1,1,0,
        })

    local vpos_loc = gl.GetAttribLocation(prog, "vPosition")
    local vcol_loc = gl.GetAttribLocation(prog, "vColor")

    local vvbo = glIntv(0)
    gl.GenBuffers(1, vvbo)
    gl.BindBuffer(GL.ARRAY_BUFFER, vvbo[0])
    gl.BufferData(GL.ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.STATIC_DRAW)
    gl.VertexAttribPointer(vpos_loc, 3, GL.FLOAT, GL.FALSE, 0, nil)
    table.insert(vbos, vvbo)

    local cvbo = glIntv(0)
    gl.GenBuffers(1, cvbo)
    gl.BindBuffer(GL.ARRAY_BUFFER, cvbo[0])
    gl.BufferData(GL.ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.STATIC_DRAW)
    gl.VertexAttribPointer(vcol_loc, 3, GL.FLOAT, GL.FALSE, 0, nil)
    table.insert(vbos, cvbo)

    gl.EnableVertexAttribArray(vpos_loc)
    gl.EnableVertexAttribArray(vcol_loc)

    local quads = glUintv(3*2, {
        0,1,2,
        0,2,3,
    })
    local qvbo = glIntv(0)
    gl.GenBuffers(1, qvbo)
    gl.BindBuffer(GL.ELEMENT_ARRAY_BUFFER, qvbo[0])
    gl.BufferData(GL.ELEMENT_ARRAY_BUFFER, ffi.sizeof(quads), quads, GL.STATIC_DRAW)
    table.insert(vbos, qvbo)
end

function julia6quad_scene.initGL()
    local vaoId = ffi.new("int[1]")
    gl.GenVertexArrays(1, vaoId)
    vao = vaoId[0]
    gl.BindVertexArray(vao)

    prog = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = basic_frag,
        })

    init_cube_attributes()
    gl.BindVertexArray(0)
end

function julia6quad_scene.exitGL()
    gl.BindVertexArray(vao)
    for _,v in pairs(vbos) do
        gl.DeleteBuffers(1,v)
    end
    vbos = {}
    gl.DeleteProgram(prog)
    local vaoId = ffi.new("GLuint[1]", vao)
    gl.DeleteVertexArrays(1, vaoId)
end

function julia6quad_scene.render_for_one_eye(view, proj)
    local umv_loc = gl.GetUniformLocation(prog, "mvmtx")
    local upr_loc = gl.GetUniformLocation(prog, "prmtx")
    local ugt_loc = gl.GetUniformLocation(prog, "iGlobalTime")
    local utw_loc = gl.GetUniformLocation(prog, "tweak")
    gl.Enable(GL.BLEND)
    gl.BlendFunc(GL.SRC_ALPHA, GL.ONE_MINUS_SRC_ALPHA)
    gl.UseProgram(prog)
    gl.UniformMatrix4fv(upr_loc, 1, GL.FALSE, glFloatv(16, proj))
    gl.UniformMatrix4fv(umv_loc, 1, GL.FALSE, glFloatv(16, view))
    gl.Uniform1f(ugt_loc, g_time)
    gl.Uniform2f(utw_loc, julia6quad_scene.tweakx, julia6quad_scene.tweaky)
    gl.BindVertexArray(vao)
    gl.DrawElements(GL.TRIANGLES, 3*2, GL.UNSIGNED_INT, nil)
    gl.BindVertexArray(0)
    gl.UseProgram(0)
    gl.Disable(GL.BLEND)
end

function julia6quad_scene.timestep(absTime, dt)
    g_time = absTime
end

return julia6quad_scene
