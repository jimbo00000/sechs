-- sunset.lua
sunset = {}

local openGL = require("opengl")
local ffi = require("ffi")
local sf = require("util.shaderfunctions")

local glIntv = ffi.typeof('GLint[?]')
local glUintv = ffi.typeof('GLuint[?]')
local glFloatv = ffi.typeof('GLfloat[?]')

local vao = 0
local rwwtt_prog = 0
local vbos = {}

local globalTime = 0

sunset.yOffset = .5
sunset.sunAlt = 0
sunset.washout = 1.

local rm_vert = [[
#version 330

in vec4 vPosition;
out vec2 vfUV;

uniform float yUVoffset;

void main()
{
    vfUV = vPosition.xy + vec2(0., yUVoffset);
    gl_Position = vPosition;
}
]]

local rm_frag = [[
// raymarch.frag
#version 330

in vec2 vfUV;
out vec4 fragColor;

uniform float uSunAlt;
uniform float washout;
uniform mat4 mvmtx;
uniform mat4 prmtx;

// https://www.shadertoy.com/view/lss3DS
// Created by Even Entem alias ThiSpawn - 2013
// License Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.

// the base unit is the meter !

//===============================================================================================
// MATH CONSTANTS
//===============================================================================================

const float PI = 3.14159265358979323846;
const float PI_2 = 1.57079632679489661923;
const float PI_4 = 0.785398163397448309616;

//===============================================================================================
// COLOR SPACE TOOLS
//===============================================================================================

const mat3 mRGB2XYZ = mat3(0.5141364, 0.3238786 , 0.16036376,
                           0.265068 , 0.67023428, 0.06409157,
                           0.0241188, 0.1228178 , 0.84442666);

const mat3 mXYZ2RGB = mat3( 2.5651, -1.1665, -0.3986,
                           -1.0217,  1.9777,  0.0439,
                            0.0753, -0.2543,  1.1892);

vec3 rgb_to_yxy(vec3 cRGB)
{
    vec3 cYxy;
    vec3 cXYZ = mRGB2XYZ * cRGB;
    cYxy.r = cXYZ.g;
    float temp = dot(vec3(1.0, 1.0, 1.0), cXYZ.rgb); 
    cYxy.gb = cXYZ.rg / temp;
    return cYxy;
}

vec3 yxy_to_rgb(vec3 cYxy)
{
    vec3 cXYZ = vec3(cYxy.r * cYxy.g / cYxy.b,
                     cYxy.r,
                     cYxy.r * (1.0 - cYxy.g - cYxy.b) / cYxy.b);
    return mXYZ2RGB * cXYZ;
}

const vec3 vGammaPowerInverse = vec3(2.2);
const vec3 vGammaPower = vec3(0.454545);

vec3 linear_to_gamma(vec3 c)
{
    return pow(clamp(c, 0.0, 1.0), vGammaPower);
}

vec3 gamma_to_linear(vec3 c)
{
    return pow(clamp(c, 0.0, 1.0), vGammaPowerInverse);
}

//===============================================================================================
// SCENE PARAMETERS
//===============================================================================================

// North is pointed by x
// East is pointed by z
// Sea Level : y = 0.0

struct slight // spotlight 
{
    vec3 pos;   // position
    vec3 d;     // diameter
    float i;    // intensity
    vec3 c;     // color
};

// TODO : fix misplaced stuff from sun_t to planet_t
    
struct planet_t
{
    float rs;       // sea level radius
    float ra;       // atmosphere radius
    vec3  beta_r;   // rayleigh scattering coefs at sea level
    vec3  beta_m;   // mie scattering coefs at sea level
    float sh_r;     // rayleigh scale height
    float sh_m;     // mie scale height
};

planet_t earth = planet_t(
    6360.0e3, 6420.0e3,
    vec3(5.5e-6, 13.0e-6, 22.1e-6),
    vec3(21.0e-6),
    7994.0, 1200.0
);

struct sun_t
{  
    float i;        // sun intensity
    float mc;       // mean cosine
    float azi;      // azimuth
    float alt;      // altitude
    float ad;       // angular diameter (between 0.5244 and 5.422 for our sun)
    vec3 color;
};

sun_t sun = sun_t(
    20.0,
    0.76,
    4.4,
    PI_2,
    0.53,
    vec3(1.0, 1.0, 1.0)
);

// Use http://www.esrl.noaa.gov/gmd/grad/solcalc/azel.html to get azimuth and altitude
// of our sun at a specific place and time

//===============================================================================================
// RAY MARCHING STUFF
//===============================================================================================

// pred : rd is normalized
bool intersect_with_atmosphere(in vec3 ro, in vec3 rd, in planet_t planet, out float tr)
{
    float c = length(ro); // distance from center of the planet :)
    vec3 up_dir = ro / c;
    float beta = PI - acos(dot(rd, up_dir)); 
    float sb = sin(beta);
    float b = planet.ra;
    float bt = planet.rs - 10.0;
    
    tr = sqrt((b * b) - (c * c) * (sb * sb)) + c * cos(beta); // sinus law
    
    if (sqrt((bt * bt) - (c * c) * (sb * sb)) + c * cos(beta) > 0.0)
        return false;
    
    return true;
}

const int SKYLIGHT_NB_VIEWDIR_SAMPLES = 6; //12;
const int SKYLIGHT_NB_SUNDIR_SAMPLES = 4; //8;

float compute_sun_visibility(in sun_t sun, float alt)
{
    float vap = 0.0;
    float h, a;
    float vvp = clamp((0.5 + alt / sun.ad), 0.0, 1.0); // vertically visible percentage
    if (vvp == 0.0)
        return 0.0;
    else if (vvp == 1.0)
        return 1.0;
        
    bool is_sup;
    
    if (vvp > 0.5)
    {
        is_sup = true;
        h = (vvp - 0.5) * 2.0;
    }
    else
    {
        is_sup = false;
        h = (0.5 - vvp) * 2.0;
    }
    
    float alpha = acos(h) * 2.0;
    a = (alpha - sin(alpha)) / (2.0 * PI);
    
    if (is_sup)
        vap = 1.0 - a;
    else
        vap = a;

    return vap;
}

// pred : rd is normalized
vec3 compute_sky_light(in vec3 ro, in vec3 rd, in planet_t planet, in sun_t sun)
{
    float t1;
    
    if (!intersect_with_atmosphere(ro, rd, planet, t1) || t1 < 0.0)
        return vec3(0.0);
    
    float sl = t1 / float(SKYLIGHT_NB_VIEWDIR_SAMPLES); // seg length
    float t = 0.0;
    
    float calt = cos(sun.alt);
    vec3 sun_dir = vec3(cos(sun.azi) * calt,
                        sin(sun.alt),
                        sin(sun.azi) * calt);
    float mu = dot(rd, sun_dir);
    float mu2 = mu * mu;
    float mc2 = sun.mc * sun.mc;
    
    // rayleigh stuff
    vec3 sumr = vec3(0.0);
    float odr = 0.0; // optical depth
    float phase_r = (3.0 / (16.0 * PI)) * (1.0 + mu2);
    
    // mie stuff
    vec3 summ = vec3(0.0);
    float odm = 0.0; // optical depth
    float phase_m = ((3.0 / (8.0 * PI)) * ((1.0 - mc2) * (1.0 + mu2))) /
                    ((2.0 + mc2) * pow(1.0 + mc2 - 2.0 * sun.mc * mu, 1.5));
    
    for (int i = 0; i < SKYLIGHT_NB_VIEWDIR_SAMPLES; ++i)
    {
        vec3 sp = ro + rd * (t + 0.5 * sl);
        float h = length(sp) - planet.rs;
        float hr = exp(-h / planet.sh_r) * sl;
        odr += hr;
        float hm = exp(-h / planet.sh_m) * sl;
        odm += hm;
        float tm;
        float sp_alt = PI_2 - asin(planet.rs / length(sp));
        sp_alt += acos(normalize(sp).y) + sun.alt;
        float coef = compute_sun_visibility(sun, sp_alt);
        if (intersect_with_atmosphere(sp, sun_dir, planet, tm) || coef > 0.0)
        {
            float sll = tm / float(SKYLIGHT_NB_SUNDIR_SAMPLES);
            float tl = 0.0;
            float odlr = 0.0, odlm = 0.0;
            for (int j = 0; j < SKYLIGHT_NB_SUNDIR_SAMPLES; ++j)
            {
                vec3 spl = sp + sun_dir * (tl + 0.5 * sll);
                float spl_alt = PI_2 - asin(planet.rs / length(spl));
                spl_alt += acos(normalize(spl).y) + sun.alt;
                float coefl = compute_sun_visibility(sun, spl_alt);
                float hl = length(spl) - planet.rs;
                odlr += exp(-hl / planet.sh_r) * sll * (1.0 - log(coefl + 0.000001));
                odlm += exp(-hl / planet.sh_m) * sll * (1.0 - log(coefl + 0.000001));
                tl += sll;
            }
            vec3 tau = planet.beta_r * (odr + odlr) + planet.beta_m * 1.05 * (odm + odlm);
            vec3 attenuation = vec3(exp(-tau.x), exp(-tau.y), exp(-tau.z));
            sumr +=  hr * attenuation * coef;
            summ +=  hm * attenuation * coef;
        }
        t += sl;
    }
    
    return sun.i * (sumr * phase_r * planet.beta_r + summ * phase_m * planet.beta_m);
}

//===============================================================================================
// MAIN
//===============================================================================================
#if 0
void mainImage( out vec4 fragColor, in vec2 fragCoord )
{   
    vec2 q = fragCoord.xy / iResolution.xy;
    vec2 p = -1.0 + 2.0*q;
    p.x *= iResolution.x / iResolution.y;

    vec3 c_position = vec3(0.0, 0.1, 0.0);
    vec3 c_lookat   = vec3(0.0, 0.8, -1.0);
    vec3 c_updir    = vec3(0.0, 1.0, 0.0);
    
    vec3 view_dir = normalize(c_lookat - c_position);
    
    vec3 uu = normalize(cross(view_dir, c_updir));
    vec3 vv = normalize(cross(uu, view_dir));
    vec3 rd = normalize(p.x * uu + p.y * vv + 1.2 * view_dir);
    
    sun.alt = uSunAlt;

    vec3 gp = c_position + vec3(0.0, earth.rs + 1.0, 0.0);
    vec3 res = compute_sky_light(gp, rd, earth, sun);
    
    //================================
    // POST EFFECTS
    
    if( q.x > 0.0 )
    {
        float crush = 0.6;
        float frange = 7.8;
        float exposure = 60.0;
        res = log2(1.0+res*exposure);
        res = smoothstep(crush, frange, res);
        res = res*res*res*(res*(res*6.0 - 15.0) + 10.0);
    }
    
    // switch to gamma space before perceptual tweaks
    res = linear_to_gamma(res);
    
    // vignetting
    // tensor product of the parametric curve defined by (4(t-t²))^0.1
    //res *= 0.5 + 0.5 * pow(16.0 * q.x * q.y * (1.0 - q.x) * (1.0 - q.y), 0.1);
    
    
    fragColor = vec4(res, 1.0);
}
#endif

vec3 getSceneColor( in vec3 ro, in vec3 rd, inout float depth )
{
    vec3 c_position = vec3(0.0, 0.1, 0.0);

    sun.alt = uSunAlt;

    vec3 gp = c_position + vec3(0.0, earth.rs + 1.0, 0.0);
    vec3 res = compute_sky_light(gp, rd, earth, sun);

    //================================
    // POST EFFECTS
    
    {
        float crush = 0.6;
        float frange = 7.8;
        float exposure = 60.0;
        res = log2(1.0+res*exposure);
        res = smoothstep(crush, frange, res);
        res = res*res*res*(res*(res*6.0 - 15.0) + 10.0);
    }
    
    // switch to gamma space before perceptual tweaks
    res = linear_to_gamma(res);
    
    return washout * res;
}

///////////////////////////////////////////////////////////////////////////////
// Patch in the Rift's heading to raymarch shader writing out color and depth.
// http://blog.hvidtfeldts.net/

// Translate the origin to the camera's location in world space.
vec3 getEyePoint(mat4 mvmtx)
{
    vec3 ro = -mvmtx[3].xyz;
    return ro;
}

// Construct the usual eye ray frustum oriented down the negative z axis.
// http://antongerdelan.net/opengl/raycasting.html
vec3 getRayDirection(vec2 uv)
{
    vec4 ray_clip = vec4(uv.x, uv.y, -1., 1.);
    vec4 ray_eye = inverse(prmtx) * ray_clip;
    return normalize(vec3(ray_eye.x, ray_eye.y, -1.));
}

void main()
{
    vec2 uv = vfUV;
    vec3 ro = getEyePoint(mvmtx);
    vec3 rd = getRayDirection(uv);

    ro *= mat3(mvmtx);
    rd *= mat3(mvmtx);

    float depth = 9999.0;
    vec3 col = getSceneColor(ro, rd, depth);
    fragColor = vec4(col, 1.0);
    
    // Write to depth buffer
    vec3 eyeFwd = vec3(0.,0.,-1.) * mat3(mvmtx);
    float eyeHitZ = -depth * dot(rd, eyeFwd);
    float p10 = prmtx[2].z;
    float p11 = prmtx[3].z;
    // A little bit of algebra...
    float ndcDepth = -p10 + -p11 / eyeHitZ;
    float dep = ((gl_DepthRange.diff * ndcDepth) + gl_DepthRange.near + gl_DepthRange.far) / 2.0;

    //gl_FragDepth = dep;
}
]]

local function make_quad_vbos(prog)
    vbos = {}

    local vvbo = glIntv(0)
    gl.GenBuffers(1, vvbo)
    table.insert(vbos, vvbo)

    gl.BindBuffer(GL.ARRAY_BUFFER, vvbo[0])
    local verts = glFloatv(4*2, {
        -1,-1,
        1,-1,
        1,1,
        -1,1,
        })
    gl.BufferData(GL.ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.STATIC_DRAW)

    local vpos_loc = gl.GetAttribLocation(prog, "vPosition")
    gl.VertexAttribPointer(vpos_loc, 2, GL.FLOAT, GL.FALSE, 0, nil)
    gl.EnableVertexAttribArray(vpos_loc)

    local quads = glUintv(3*2, {
        0,1,2,
        0,2,3,
    })
    local qvbo = glIntv(0)
    gl.GenBuffers(1, qvbo)
    table.insert(vbos, qvbo)

    gl.BindBuffer(GL.ELEMENT_ARRAY_BUFFER, qvbo[0])
    gl.BufferData(GL.ELEMENT_ARRAY_BUFFER, ffi.sizeof(quads), quads, GL.STATIC_DRAW)

    return vbos
end

function sunset.initGL()
    rwwtt_prog = sf.make_shader_from_source({
        vsrc = rm_vert,
        fsrc = rm_frag,
        })
    
    local vaoId = ffi.new("int[1]")
    gl.GenVertexArrays(1, vaoId)
    vao = vaoId[0]
    gl.BindVertexArray(vao)
    vbos = make_quad_vbos(rwwtt_prog)
    gl.BindVertexArray(0)
end

function sunset.exitGL()
    gl.BindVertexArray(vao)
    for _,v in pairs(vbos) do
        gl.DeleteBuffers(1,v)
    end
    vbos = {}
    gl.DeleteProgram(rwwtt_prog)
    local vaoId = ffi.new("GLuint[1]", vao)
    gl.DeleteVertexArrays(1, vaoId)
end

function sunset.render_for_one_eye(mview, proj)
    local prog = rwwtt_prog
    gl.PolygonMode(GL.FRONT_AND_BACK, GL.FILL)
    gl.UseProgram(rwwtt_prog)

    local uyo_loc = gl.GetUniformLocation(prog, "yUVoffset")
    gl.Uniform1f(uyo_loc, sunset.yOffset)

    local ugt_loc = gl.GetUniformLocation(prog, "uSunAlt")
    local PI_4 = 0.785398163397448309616
    -- [-.09, .84]
    local sunalt = -0.1 + 1.2 * PI_4 * (0.5 + math.cos(0.38 * globalTime) / 2.0);
    if sunset.sunAlt then sunalt = sunset.sunAlt end
    gl.Uniform1f(ugt_loc, sunalt)

    local uwo_loc = gl.GetUniformLocation(prog, "washout")
    gl.Uniform1f(uwo_loc, sunset.washout)

    local umv_loc = gl.GetUniformLocation(prog, "mvmtx")
    gl.UniformMatrix4fv(umv_loc, 1, GL.FALSE, glFloatv(16, mview))
    local upr_loc = gl.GetUniformLocation(prog, "prmtx")
    gl.UniformMatrix4fv(upr_loc, 1, GL.FALSE, glFloatv(16, proj))

    gl.BindVertexArray(vao)
    gl.DrawElements(GL.TRIANGLES, 3*2, GL.UNSIGNED_INT, nil)
    gl.BindVertexArray(0)
    gl.UseProgram(0)
end

function sunset.timestep(absTime, dt)
    globalTime = absTime
end

return sunset
