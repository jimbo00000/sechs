-- anemone2_scene.lua
anemone2_scene = {}

local openGL = require("opengl")
local ffi = require("ffi")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")

local glIntv     = ffi.typeof('GLint[?]')
local glUintv    = ffi.typeof('GLuint[?]')
local glFloatv   = ffi.typeof('GLfloat[?]')

local vao = 0
local vbos = {}
local progs = {}
local g_time = 0.0
local numInstances = 256

anemone2_scene.anemscale = 2
anemone2_scene.armrot_x = 0
anemone2_scene.armrot_y = 0
anemone2_scene.pan = -30

local basic_vert = [[
#version 330
#line 19

in vec4 instancePosition;
in vec4 instanceOrientation;

out vec3 vfColor;

uniform mat4 mvmtx;
uniform mat4 prmtx;
uniform vec2 uArmRot;
uniform int uNumBasePairs;

mat3 rotx(float th)
{
    return mat3(
        1., 0., 0.,
        0., cos(th), -sin(th),
        0., sin(th), cos(th)
        );
}

mat3 roty(float th)
{
    return mat3(
        cos(th), 0., sin(th),
        0., 1., 0.,
        -sin(th), 0., cos(th)
        );
}

mat3 rotz(float th)
{
    return mat3(
        cos(th), -sin(th), 0.,
        sin(th), cos(th), 0.,
        0., 0., 1.
        );
}

// Quaternion rotation of a vector
vec3 qtransform(vec4 q, vec3 v)
{
    return v + 2.0*cross(cross(v, q.xyz) + q.w*v, q.xyz);
}

#define PI 3.14159265359

void main()
{
    const int n = 18;
    int i = gl_VertexID % n;
    int j = gl_VertexID / n;

    const float a = .5;
    const float b = .5 * sqrt(3.);
    const float c = 1.7;
    const vec3 hexpts[n] = vec3[n](
        // A hexagon
        vec3(0., 0., 0.),
        vec3(1., 0., 0.),
        vec3(a, b, 0.),

        vec3(0., 0., 0.),
        vec3(a, b, 0.),
        vec3(-a, b, 0.),

        vec3(0., 0., 0.),
        vec3(-a, b, 0.),
        vec3(-1, 0, 0.),

        vec3(0., 0., 0.),
        vec3(-1, 0, 0.),
        vec3(-a, -b, 0.),

        vec3(0., 0., 0.),
        vec3(-a, -b, 0.),
        vec3(a, -b, 0.),

        vec3(0., 0., 0.),
        vec3(a, -b, 0.),
        vec3(1, 0, 0.)
    );
    const vec3 tubepts[n] = vec3[n](
        // a tube
        vec3(1., 0., -c),
        vec3(1., 0., c),
        vec3(-a, b, -c),

        vec3(-a, b, -c),
        vec3(1., 0., c),
        vec3(-a, b, c),

        vec3(-a, b, -c),
        vec3(-a, b, c),
        vec3(-a, -b, c),

        vec3(-a, b, -c),
        vec3(-a, -b, c),
        vec3(-a, -b, -c),

        vec3(-a, -b, -c),
        vec3(-a, -b, c),
        vec3(1., 0., -c),

        vec3(1., 0., -c),
        vec3(-a, -b, c),
        vec3(1., 0., c)
    );

    int type = 2;
    int o = j % type;
    int p = j / type;

    bool hex = ((o & 1) == 0);

    float sz = .1;
    vec3 pos = sz * hexpts[i].yxz;
    vec3 col = hexpts[i];

    // radial anemone
    int order = 12;
    int k = j % order;
    int l = j / order;

    pos *= 3.;

    pos.z -= .5;
    float r = 2. * PI / float(order);

    for (int f=0; f<l; ++f)
    {
        pos *= rotx(uArmRot.x);
        pos *= rotz(uArmRot.y);
        pos.y += .5;
        pos *= .8;
    }
    pos *= roty(float(k) * r);

    vec3 radcol = vec3(1., 0., 0.) * rotz(float(k));
    col = mix(col, radcol, .5);

    // Instance
    vec4 q = normalize(instanceOrientation);
    vec3 ipos = instancePosition.xyz;

    pos = qtransform(q, pos);
    pos += ipos;

    vfColor = col;
    gl_Position = prmtx * mvmtx * vec4(pos,1.);
}
]]

local basic_frag = [[
#version 330
#line 212

in vec3 vfColor;
out vec4 fragColor;

// Official HSV to RGB conversion 
vec3 hsv2rgb( in vec3 c )
{
    vec3 rgb = clamp( abs(mod(c.x*6.0+vec3(0.0,4.0,2.0),6.0)-3.0)-1.0, 0.0, 1.0 );
    return c.z * mix( vec3(1.0), rgb, c.y);
}

void main()
{
    vec2 uv = vfColor.xy;
    float a = atan(uv.x,uv.y);
    a =(a / 3.14159*0.5)+1.0 ;
    float d = length(uv);
    vec3 col = hsv2rgb(vec3(a, d, 1.));
    col = pow(col, vec3(.7));
    fragColor = vec4(col, 1.);
}
]]

local function init_instance_attributes()
    local prog = progs.draw
    local sz = 4 * numInstances * ffi.sizeof('GLfloat') -- xyzw

    local insp_loc = gl.GetAttribLocation(prog, "instancePosition")
    local ipvbo = glIntv(0)
    gl.GenBuffers(1, ipvbo)
    gl.BindBuffer(GL.ARRAY_BUFFER, ipvbo[0])
    gl.BufferData(GL.ARRAY_BUFFER, sz, nil, GL.STATIC_COPY)
    gl.VertexAttribPointer(insp_loc, 4, GL.FLOAT, GL.FALSE, 0, nil)
    vbos.inst_positions = ipvbo
    gl.VertexAttribDivisor(insp_loc, 1)
    gl.EnableVertexAttribArray(insp_loc)

    local inso_loc = gl.GetAttribLocation(prog, "instanceOrientation")
    local iovbo = glIntv(0)
    gl.GenBuffers(1, iovbo)
    gl.BindBuffer(GL.ARRAY_BUFFER, iovbo[0])
    gl.BufferData(GL.ARRAY_BUFFER, sz, nil, GL.STATIC_DRAW)
    gl.VertexAttribPointer(inso_loc, 4, GL.FLOAT, GL.FALSE, 0, nil)
    vbos.inst_orientations = iovbo
    gl.VertexAttribDivisor(inso_loc, 2)
    gl.EnableVertexAttribArray(inso_loc)

    -- Velocities and angular velocities are not drawn; used only by compute.
    local vvbo = glIntv(0)
    gl.GenBuffers(1, vvbo)
    gl.BindBuffer(GL.ARRAY_BUFFER, vvbo[0])
    gl.BufferData(GL.ARRAY_BUFFER, sz, nil, GL.STATIC_DRAW)
    vbos.inst_velocities = vvbo

    -- Angular velocities are not drawn; used only by compute.
    local avvbo = glIntv(0)
    gl.GenBuffers(1, avvbo)
    gl.BindBuffer(GL.ARRAY_BUFFER, avvbo[0])
    gl.BufferData(GL.ARRAY_BUFFER, sz, nil, GL.STATIC_DRAW)
    vbos.inst_angularvelocities = avvbo
end

--[[
    Scatter instance positions randomly in particles array.
]]
local randomize_instance_positions_comp_src = [[
#version 430

layout(local_size_x=256) in;
layout(std430, binding=0) buffer pblock { vec4 positions[]; };
layout(std430, binding=1) buffer oblock { vec4 orientations[]; };
layout(std430, binding=2) buffer vblock { vec4 velocities[]; };
layout(std430, binding=3) buffer avblock { vec4 angularVelocities[]; };
uniform int numInstances;

float hash( float n ) { return fract(sin(n)*43758.5453); }

void main()
{
    uint index = gl_GlobalInvocationID.x;
    if (index >= numInstances)
        return;
    float fi = float(index+1) / float(numInstances);

    vec3 pos = 
          vec3(
               30.* (-.5+hash(fi)),
               -2.,
               30.* (-.5+hash(fi*17.))
          );
    positions[index] = vec4(pos, 1.);

    orientations[index] = vec4(0., 0., 0., 1.);
    velocities[index] = vec4(0.);
    angularVelocities[index] = vec4(0.);

#if 1
    if (index == 0)
    {
        // Manually reset the first instance
        positions[0] = vec4(vec3(0., -2., 0.), 1.);
        orientations[0] = vec4(0., 0., 0., 1.);
        velocities[0] = vec4(0.);
        angularVelocities[0] = vec4(0., 0., 0., 1.);
    }
#endif
}
]]
local function randomize_instance_positions()
    if progs.scatter_instances == nil then
        progs.scatter_instances = sf.make_shader_from_source({
            compsrc = randomize_instance_positions_comp_src,
            })
    end
    local pvbo = vbos.inst_positions
    local ovbo = vbos.inst_orientations
    local vvbo = vbos.inst_velocities
    local avvbo = vbos.inst_angularvelocities
    gl.BindBufferBase(GL.SHADER_STORAGE_BUFFER, 0, pvbo[0])
    gl.BindBufferBase(GL.SHADER_STORAGE_BUFFER, 1, ovbo[0])
    gl.BindBufferBase(GL.SHADER_STORAGE_BUFFER, 2, vvbo[0])
    gl.BindBufferBase(GL.SHADER_STORAGE_BUFFER, 3, avvbo[0])
    local prog = progs.scatter_instances
    local function uni(name) return gl.GetUniformLocation(prog, name) end
    gl.UseProgram(prog)
    gl.Uniform1i(uni("numInstances"), numInstances)
    gl.DispatchCompute(numInstances/256+1, 1, 1)
    gl.UseProgram(0)
end

--[[
    Apply angular velocity every timestep
]]
local instance_timestep_comp_src = [[
#version 430

layout(local_size_x=256) in;
layout(std430, binding=0) buffer pblock { vec4 positions[]; };
layout(std430, binding=1) buffer oblock { vec4 orientations[]; };
layout(std430, binding=2) buffer vblock { vec4 velocities[]; };
layout(std430, binding=3) buffer avblock { vec4 angularVelocities[]; };
uniform int numInstances;
uniform float dt;

// http://www.gamedev.net/reference/articles/article1095.asp
//w = w1w2 - x1x2 - y1y2 - z1z2
//x = w1x2 + x1w2 + y1z2 - z1y2
//y = w1y2 + y1w2 + z1x2 - x1z2
//z = w1z2 + z1w2 + x1y2 - y1x2
// Remember the w coordinate is last!
vec4 qconcat3(vec4 a, vec4 b)
{
    return vec4(
        a.w*b.x + a.x*b.w + a.y*b.z - a.z*b.y,
        a.w*b.y + a.y*b.w + a.z*b.x - a.x*b.z,
        a.w*b.z + a.z*b.w + a.x*b.y - a.y*b.x,
        a.w*b.w - a.x*b.x - a.y*b.y - a.z*b.z
        );
}

void main()
{
    uint index = gl_GlobalInvocationID.x;
    if (index >= numInstances)
        return;

    // Position
    vec4 p = positions[index];
    vec3 v = velocities[index].xyz;
    p.xyz += dt * v.xyz;

    positions[index] = p;
    velocities[index].xyz = v;

    // Rotation
    //vec4 q = orientations[index];
    //q = normalize(qconcat3(q, angularVelocities[index]));
    //orientations[index] = q;
}
]]
local function timestep_instances(dt)
    if progs.step_instances == nil then
        progs.step_instances = sf.make_shader_from_source({
            compsrc = instance_timestep_comp_src,
            })
    end
    local prog = progs.step_instances
    local function uni(name) return gl.GetUniformLocation(prog, name) end

    local pvbo = vbos.inst_positions
    local ovbo = vbos.inst_orientations
    local vvbo = vbos.inst_velocities
    local avvbo = vbos.inst_angularvelocities
    gl.BindBufferBase(GL.SHADER_STORAGE_BUFFER, 0, pvbo[0])
    gl.BindBufferBase(GL.SHADER_STORAGE_BUFFER, 1, ovbo[0])
    gl.BindBufferBase(GL.SHADER_STORAGE_BUFFER, 2, vvbo[0])
    gl.BindBufferBase(GL.SHADER_STORAGE_BUFFER, 3, avvbo[0])
    gl.UseProgram(prog)
    gl.Uniform1i(uni("numInstances"), numInstances)
    gl.Uniform1f(uni("dt"), dt)
    gl.DispatchCompute(numInstances/256+1, 1, 1)
    gl.UseProgram(0)
end



function anemone2_scene.initGL()
    progs.draw = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = basic_frag,
        })

    local vaoId = ffi.new("int[1]")
    gl.GenVertexArrays(1, vaoId)
    vao = vaoId[0]
    gl.BindVertexArray(vao)
    do
        init_instance_attributes()
        randomize_instance_positions()
    end
    gl.BindVertexArray(0)

end

function anemone2_scene.exitGL()
    gl.BindVertexArray(vao)
    for _,v in pairs(vbos) do
        gl.DeleteBuffers(1,v)
    end
    vbos = {}

    for _,p in pairs(progs) do
        gl.DeleteProgram(p)
    end
    progs = {}

    local vaoId = ffi.new("GLuint[1]", vao)
    gl.DeleteVertexArrays(1, vaoId)
end

function anemone2_scene.render_for_one_eye(view, proj)
    local prog = progs.draw
    local umv_loc = gl.GetUniformLocation(prog, "mvmtx")
    local upr_loc = gl.GetUniformLocation(prog, "prmtx")
    local uar_loc = gl.GetUniformLocation(prog, "uArmRot")
    local ubp_loc = gl.GetUniformLocation(prog, "uNumBasePairs")
    gl.UseProgram(prog)

    local m = {}
    for i=1,16 do m[i] = view[i] end
    local s = .8
    mm.glh_translate(m, 0,0,0)
    local t = anemone2_scene.pan
    mm.glh_translate(m, 0,t,0)
    mm.glh_scale(m, s,s,s)

    gl.UniformMatrix4fv(umv_loc, 1, GL.FALSE, glFloatv(16, m))
    gl.UniformMatrix4fv(upr_loc, 1, GL.FALSE, glFloatv(16, proj))
    local x,y = anemone2_scene.armrot_x, anemone2_scene.armrot_y
    gl.Uniform2f(uar_loc, x, y)

    local basePairs = 32*8
    gl.Uniform1i(ubp_loc, basePairs)
    gl.Disable(GL.CULL_FACE)
    gl.BindVertexArray(vao)
    gl.DrawArraysInstanced(GL.TRIANGLES, 0, 6*3*2*2*basePairs, numInstances)
    gl.BindVertexArray(0)
    gl.UseProgram(0)
end

function anemone2_scene.timestep(absTime, dt)
    g_time = absTime
    timestep_instances(dt+.000000001)
end

function anemone2_scene.keypressed(ch)
    local reinit = false
    if ch == '-' then
        numInstances = numInstances / 2
        reinit = true
    elseif ch == '=' then
        numInstances = numInstances * 2
        reinit = true
    end

    if reinit then
        print(numInstances)
        anemone2_scene.exitGL()
        anemone2_scene.initGL()
    end
end

return anemone2_scene
