-- dna_scene.lua
dna_scene = {}

local openGL = require("opengl")
local ffi = require("ffi")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")

local glIntv     = ffi.typeof('GLint[?]')
local glUintv    = ffi.typeof('GLuint[?]')
local glFloatv   = ffi.typeof('GLfloat[?]')

local vao = 0
local vbos = {}
local progs = {}
local g_time = 0.0
local numInstances = 128

dna_scene.zipper = 1
dna_scene.dnascale = 1
dna_scene.rna = 0

local basic_vert = [[
#version 330
#line 24

in vec4 instancePosition;
in vec4 instanceOrientation;

out vec4 vfColor;

uniform mat4 mvmtx;
uniform mat4 prmtx;
uniform float uPhase;
uniform int uNumBasePairs;
uniform float uRna;

mat3 rotx(float th)
{
    return mat3(
        1., 0., 0.,
        0., cos(th), -sin(th),
        0., sin(th), cos(th)
        );
}

mat3 roty(float th)
{
    return mat3(
        cos(th), 0., sin(th),
        0., 1., 0.,
        -sin(th), 0., cos(th)
        );
}

mat3 rotz(float th)
{
    return mat3(
        cos(th), -sin(th), 0.,
        sin(th), cos(th), 0.,
        0., 0., 1.
        );
}

float hash( float n ) { return fract(sin(n)*43758.5453); }
float rand(vec2 n)
{ 
    return fract(sin(dot(n, vec2(12.9898, 4.1414))) * 43758.5453);
}

// Quaternion rotation of a vector
vec3 qtransform(vec4 q, vec3 v)
{
    return v + 2.0*cross(cross(v, q.xyz) + q.w*v, q.xyz);
}

#define PI 3.14159265359

void main()
{
    const float a = .5;
    const float b = .5 * sqrt(3.);
    const float c = 1.7;
    const int n = 18;

    const vec3 tubepts[n] = vec3[n](
        // a tube
        vec3(1., 0., -c),
        vec3(1., 0., c),
        vec3(-a, b, -c),

        vec3(-a, b, -c),
        vec3(1., 0., c),
        vec3(-a, b, c),

        vec3(-a, b, -c),
        vec3(-a, b, c),
        vec3(-a, -b, c),

        vec3(-a, b, -c),
        vec3(-a, -b, c),
        vec3(-a, -b, -c),

        vec3(-a, -b, -c),
        vec3(-a, -b, c),
        vec3(1., 0., -c),

        vec3(1., 0., -c),
        vec3(-a, -b, c),
        vec3(1., 0., c)
    );
    int i = gl_VertexID % n;

    // calculate rotation about 0
    int triIdx = i % 3;
    const float ri = 2.*PI/6.;

    vec3 hp3 = vec3(0.);
    if (triIdx == 1)
    {
        hp3 = vec3(0.,1.,0.);
    }
    else if (triIdx == 2)
    {
        hp3 = vec3(0.,1.,0.) * rotz(ri);
        hp3 = normalize(hp3);
    }
    hp3 *= rotz(ri * float(i / 3));
    hp3 *= rotz(PI/6.);


    vec3 hexpt = vec3(hp3.xy, 0.);

    vec3 pos = .1 * hexpt.yxz;
    vfColor = vec4(hexpt.xy, 0., 0.);


    int j = gl_VertexID / n;

    // double helix
    int p = j / 2;
    int k = p % 2;
    int l = p / 2;

    if (((j%2) & 1) == 0)
    {
        // backbone
        pos.z = .3;
    }
    else
    {
        // base pair
        pos = .08 * tubepts[i].yxz;
        pos.z += .15;
        vec3 baseCol = vec3(
                hash(float(l)),
                hash(float(3.*l)),
                hash(float(5.*l))
                );
        if ((k&1) == 1)
        {
            baseCol = vec3(1.) - baseCol;
        }
        vfColor = vec4(baseCol, 1.);
    }

    // climb up the zipper
    const float bph = .15;
    pos.y += bph * float(l);

    // zippering animation
    float zp = .2
        * (uPhase + instancePosition.w)
        * float(uNumBasePairs);

    pos.z +=
        max(0., .1*pow(max(0.,float(l) - zp), 1.5) )
        * mix(rand(vec2( float(k), float(l) )), float(k), uRna);

    // Center zipper point on local origin
    pos.y -= bph * zp;

    // chirality
    pos.z *= 2.*float(k&1)-1.;

    pos *= roty(.3*float(l));

    // Instance
    pos = qtransform(normalize(instanceOrientation), pos);
    pos += instancePosition.xyz;

    gl_Position = prmtx * mvmtx * vec4(pos,1.);
}
]]

local basic_frag = [[
#version 330
#line 189

in vec4 vfColor;
out vec4 fragColor;

// Smooth HSV to RGB conversion
// https://www.shadertoy.com/view/MsS3Wc
vec3 hsv2rgb_smooth( in vec3 c )
{
    vec3 rgb = clamp( abs(mod(c.x*6.0+vec3(0.0,4.0,2.0),6.0)-3.0)-1.0, 0.0, 1.0 );
    rgb = rgb*rgb*(3.0-2.0*rgb); // cubic smoothing 
    return c.z * mix( vec3(1.0), rgb, c.y);
}

void main()
{
    vec2 uv = vfColor.xy;
    float a = atan(uv.x,uv.y);
    a =(a / 3.14159*0.5)+1.0 ;
    float d = length(uv);
    vec3 col = hsv2rgb_smooth(vec3(a, d, 1.));
    col = pow(col, vec3(.7));
    fragColor = vec4(mix(col, vfColor.rgb, vfColor.a), 1.);
}
]]

local function init_instance_attributes()
    local prog = progs.draw
    local sz = 4 * numInstances * ffi.sizeof('GLfloat') -- xyzw

    local insp_loc = gl.GetAttribLocation(prog, "instancePosition")
    local ipvbo = glIntv(0)
    gl.GenBuffers(1, ipvbo)
    gl.BindBuffer(GL.ARRAY_BUFFER, ipvbo[0])
    gl.BufferData(GL.ARRAY_BUFFER, sz, nil, GL.STATIC_COPY)
    gl.VertexAttribPointer(insp_loc, 4, GL.FLOAT, GL.FALSE, 0, nil)
    vbos.inst_positions = ipvbo
    gl.VertexAttribDivisor(insp_loc, 1)
    gl.EnableVertexAttribArray(insp_loc)

    local inso_loc = gl.GetAttribLocation(prog, "instanceOrientation")
    local iovbo = glIntv(0)
    gl.GenBuffers(1, iovbo)
    gl.BindBuffer(GL.ARRAY_BUFFER, iovbo[0])
    gl.BufferData(GL.ARRAY_BUFFER, sz, nil, GL.STATIC_DRAW)
    gl.VertexAttribPointer(inso_loc, 4, GL.FLOAT, GL.FALSE, 0, nil)
    vbos.inst_orientations = iovbo
    gl.VertexAttribDivisor(inso_loc, 1)
    gl.EnableVertexAttribArray(inso_loc)

    -- Velocities and angular velocities are not drawn; used only by compute.
    local vvbo = glIntv(0)
    gl.GenBuffers(1, vvbo)
    gl.BindBuffer(GL.ARRAY_BUFFER, vvbo[0])
    gl.BufferData(GL.ARRAY_BUFFER, sz, nil, GL.STATIC_DRAW)
    vbos.inst_velocities = vvbo

    -- Angular velocities are not drawn; used only by compute.
    local avvbo = glIntv(0)
    gl.GenBuffers(1, avvbo)
    gl.BindBuffer(GL.ARRAY_BUFFER, avvbo[0])
    gl.BufferData(GL.ARRAY_BUFFER, sz, nil, GL.STATIC_DRAW)
    vbos.inst_angularvelocities = avvbo
end

--[[
    Scatter instance positions randomly in particles array.
]]
local randomize_instance_positions_comp_src = [[
#version 430

layout(local_size_x=256) in;
layout(std430, binding=0) buffer pblock { vec4 positions[]; };
layout(std430, binding=1) buffer oblock { vec4 orientations[]; };
layout(std430, binding=2) buffer vblock { vec4 velocities[]; };
layout(std430, binding=3) buffer avblock { vec4 angularVelocities[]; };
uniform int numInstances;

float hash( float n ) { return fract(sin(n)*43758.5453); }

void main()
{
    uint index = gl_GlobalInvocationID.x;
    if (index >= numInstances)
        return;
    float fi = float(index+1) / float(numInstances);

    positions[index] = vec4(
        50.*
          (vec3(-.5) + vec3(hash(fi), hash(fi*3.), hash(fi*17.)) ),
         hash(fi*7.));

    orientations[index] = vec4(hash(fi*11.), hash(fi*5.), hash(fi*7.), hash(fi*19.));

    vec3 randv = vec3(hash(fi*19.), hash(fi*17.), hash(fi*7.));
    vec3 motiondir = 2.*(vec3(-.5) + randv); // isotropic, go in all directions
    motiondir = pow(length(motiondir), 3.) * normalize(motiondir);
    velocities[index] = vec4(5.*motiondir, 0.);

    vec3 randav = vec3(hash(fi*11.), hash(fi*5.), hash(fi*7.));
    vec3 rotangle = .9*(randav); // isotropic, go in all directions
    rotangle = clamp(pow(rotangle, vec3(23.)), -.001, .001);
    angularVelocities[index] = (vec4(rotangle, 1.));

#define RESET_FIRST_INSTANCE_TO_ZERO
#ifdef RESET_FIRST_INSTANCE_TO_ZERO
    if (index == 0)
    {
        // Manually reset the first instance
        positions[0] = vec4(vec3(0.), 0.);
        orientations[0] = vec4(0., 0., 0., 1.);
        velocities[0] = vec4(0.);
        angularVelocities[0] = vec4(0., 0., 0., 1.);
    }
#endif
}
]]
local function randomize_instance_positions()
    if progs.scatter_instances == nil then
        progs.scatter_instances = sf.make_shader_from_source({
            compsrc = randomize_instance_positions_comp_src,
            })
    end
    local pvbo = vbos.inst_positions
    local ovbo = vbos.inst_orientations
    local vvbo = vbos.inst_velocities
    local avvbo = vbos.inst_angularvelocities
    gl.BindBufferBase(GL.SHADER_STORAGE_BUFFER, 0, pvbo[0])
    gl.BindBufferBase(GL.SHADER_STORAGE_BUFFER, 1, ovbo[0])
    gl.BindBufferBase(GL.SHADER_STORAGE_BUFFER, 2, vvbo[0])
    gl.BindBufferBase(GL.SHADER_STORAGE_BUFFER, 3, avvbo[0])
    local prog = progs.scatter_instances
    local function uni(name) return gl.GetUniformLocation(prog, name) end
    gl.UseProgram(prog)
    gl.Uniform1i(uni("numInstances"), numInstances)
    gl.DispatchCompute(numInstances/256+1, 1, 1)
    gl.UseProgram(0)
end

--[[
    Apply angular velocity every timestep
]]
local instance_timestep_comp_src = [[
#version 430

layout(local_size_x=256) in;
layout(std430, binding=0) buffer pblock { vec4 positions[]; };
layout(std430, binding=1) buffer oblock { vec4 orientations[]; };
layout(std430, binding=2) buffer vblock { vec4 velocities[]; };
layout(std430, binding=3) buffer avblock { vec4 angularVelocities[]; };
uniform int numInstances;
uniform float dt;

// http://www.gamedev.net/reference/articles/article1095.asp
//w = w1w2 - x1x2 - y1y2 - z1z2
//x = w1x2 + x1w2 + y1z2 - z1y2
//y = w1y2 + y1w2 + z1x2 - x1z2
//z = w1z2 + z1w2 + x1y2 - y1x2
// Remember the w coordinate is last!
vec4 qconcat3(vec4 a, vec4 b)
{
    return vec4(
        a.w*b.x + a.x*b.w + a.y*b.z - a.z*b.y,
        a.w*b.y + a.y*b.w + a.z*b.x - a.x*b.z,
        a.w*b.z + a.z*b.w + a.x*b.y - a.y*b.x,
        a.w*b.w - a.x*b.x - a.y*b.y - a.z*b.z
        );
}

void main()
{
    uint index = gl_GlobalInvocationID.x;
    if (index >= numInstances)
        return;

    // Position
    vec4 p = positions[index];
    vec3 v = velocities[index].xyz;
    p.xyz += dt * v.xyz;

    positions[index] = p;
    velocities[index].xyz = v;

    // Rotation
    vec4 q = orientations[index];
    q = normalize(qconcat3(q, angularVelocities[index]));
    orientations[index] = q;
}
]]
local function timestep_instances(dt)
    if progs.step_instances == nil then
        progs.step_instances = sf.make_shader_from_source({
            compsrc = instance_timestep_comp_src,
            })
    end
    local prog = progs.step_instances
    local function uni(name) return gl.GetUniformLocation(prog, name) end

    local pvbo = vbos.inst_positions
    local ovbo = vbos.inst_orientations
    local vvbo = vbos.inst_velocities
    local avvbo = vbos.inst_angularvelocities
    gl.BindBufferBase(GL.SHADER_STORAGE_BUFFER, 0, pvbo[0])
    gl.BindBufferBase(GL.SHADER_STORAGE_BUFFER, 1, ovbo[0])
    gl.BindBufferBase(GL.SHADER_STORAGE_BUFFER, 2, vvbo[0])
    gl.BindBufferBase(GL.SHADER_STORAGE_BUFFER, 3, avvbo[0])
    gl.UseProgram(prog)
    gl.Uniform1i(uni("numInstances"), numInstances)
    gl.Uniform1f(uni("dt"), dt)
    gl.DispatchCompute(numInstances/256+1, 1, 1)
    gl.UseProgram(0)
end



function dna_scene.initGL()
    progs.draw = sf.make_shader_from_source({
        vsrc = basic_vert,
        fsrc = basic_frag,
        })

    local vaoId = ffi.new("int[1]")
    gl.GenVertexArrays(1, vaoId)
    vao = vaoId[0]
    gl.BindVertexArray(vao)
    do
        init_instance_attributes()
        randomize_instance_positions()
    end
    gl.BindVertexArray(0)

end

function dna_scene.exitGL()
    gl.BindVertexArray(vao)
    for _,v in pairs(vbos) do
        gl.DeleteBuffers(1,v)
    end
    vbos = {}

    for _,p in pairs(progs) do
        gl.DeleteProgram(p)
    end
    progs = {}

    local vaoId = ffi.new("GLuint[1]", vao)
    gl.DeleteVertexArrays(1, vaoId)
end

function dna_scene.render_for_one_eye(view, proj)
    local prog = progs.draw
    local umv_loc = gl.GetUniformLocation(prog, "mvmtx")
    local upr_loc = gl.GetUniformLocation(prog, "prmtx")
    local up_loc = gl.GetUniformLocation(prog, "uPhase")
    local ubp_loc = gl.GetUniformLocation(prog, "uNumBasePairs")
    local urn_loc = gl.GetUniformLocation(prog, "uRna")
    gl.UseProgram(prog)

    local m = {}
    for i=1,16 do m[i] = view[i] end
    local s = dna_scene.dnascale
    mm.glh_scale(m, s,s,s)

    gl.UniformMatrix4fv(umv_loc, 1, GL.FALSE, glFloatv(16, m))
    gl.UniformMatrix4fv(upr_loc, 1, GL.FALSE, glFloatv(16, proj))

    function tri(t)
        local ipart = math.modf(t, 1)
        local fpart = t - ipart
        if ipart % 2 == 1 then
            return 1-fpart
        end
        return fpart
    end
    local phase = 5 * tri(.1 * g_time)
    if dna_scene.zipper then phase = 5 * dna_scene.zipper end
    gl.Uniform1f(up_loc, phase)

    gl.Uniform1f(urn_loc, dna_scene.rna)

    local basePairs = 32*8
    gl.Uniform1i(ubp_loc, basePairs)
    gl.Disable(GL.CULL_FACE)
    gl.BindVertexArray(vao)
    gl.DrawArraysInstanced(GL.TRIANGLES, 0, 6*3*2*2*basePairs, numInstances)
    gl.BindVertexArray(0)
    gl.UseProgram(0)
end

function dna_scene.timestep(absTime, dt)
    g_time = absTime
    timestep_instances(dt+.000000001)
end

function dna_scene.keypressed(ch)
    local reinit = false
    if ch == '-' then
        numInstances = numInstances / 2
        reinit = true
    elseif ch == '=' then
        numInstances = numInstances * 2
        reinit = true
    end

    if reinit then
        print(numInstances)
        dna_scene.exitGL()
        dna_scene.initGL()
    end
end

return dna_scene
