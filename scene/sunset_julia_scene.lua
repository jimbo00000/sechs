-- sunset_julia_scene.lua

sunset_julia_scene = {}

local openGL = require("opengl")

local SS = require("scene.sunset")
local JS = require("scene.julia6quad_scene")

sunset_julia_scene.yOffset = .5
sunset_julia_scene.sunAlt = 0
sunset_julia_scene.tweakx = .5
sunset_julia_scene.tweaky = 1.05
sunset_julia_scene.washout = 1

function sunset_julia_scene.initGL()
    SS.initGL()
    JS.initGL()
end

function sunset_julia_scene.exitGL()
    SS.exitGL()
    JS.exitGL()
end

function sunset_julia_scene.render_for_one_eye(view, proj)
    SS.render_for_one_eye(view, proj)
    gl.Disable(GL.DEPTH_TEST)
    JS.render_for_one_eye(view, proj)
    gl.Enable(GL.DEPTH_TEST)
end

function sunset_julia_scene.timestep(absTime, dt)
    SS.timestep(absTime, dt)
    JS.timestep(absTime, dt)

    -- hackaround for setting child scenes' variables
    SS.yOffset = sunset_julia_scene.yOffset
    SS.sunAlt = sunset_julia_scene.sunAlt
    SS.washout = sunset_julia_scene.washout
    JS.tweakx = sunset_julia_scene.tweakx
    JS.tweaky = sunset_julia_scene.tweaky
end

return sunset_julia_scene
