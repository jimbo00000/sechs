-- vortices_scene.lua
vortices_scene = {}

local openGL = require("opengl")
local ffi = require("ffi")
local mm = require("util.matrixmath")
local sf = require("util.shaderfunctions")

local glIntv     = ffi.typeof('GLint[?]')
local glUintv    = ffi.typeof('GLuint[?]')
local glFloatv   = ffi.typeof('GLfloat[?]')

local vao = 0
local vbos = {}
local progs = {}
local numPoints = 1024*4

local ringVbos = {}
local ringSz = 128
local ringPtr = 1
local histSz = 64

local hist_vert = [[
// vortex_history.vert
#version 330

// 0 is current, 1 is in the past
layout (location = 0) in vec3 in_Pos0;
layout (location = 1) in vec3 in_Pos1;
layout (location = 2) in vec3 in_Pos2;

out vec3 v_g_Pos1;
out vec3 v_g_Pos2;

void main(void)
{
    gl_Position = vec4(in_Pos0,1);
    v_g_Pos1 = in_Pos1;
    v_g_Pos2 = in_Pos2;
}
]]

local hist_geom = [[
// vortex_history.geom
#version 330

// Set USE_TRIANGLES to 1 to enable geometry generation for history path triangles.
#define USE_TRIANGLES 1

layout(points) in;

#if USE_TRIANGLES
layout(triangle_strip, max_vertices=128) out;
// NVIDIA GTX285 hardware limitation of 128 vertices...
// But hey, at least it works. Unlike AMD Turks.
#else
layout(line_strip, max_vertices=4) out;
#endif

in vec3 v_g_Pos1[];
in vec3 v_g_Pos2[];

out vec4 g_f_DiffuseCol;

uniform mat4 mvmtx;
uniform mat4 prmtx;
uniform float recency;
uniform float recencyStep;
uniform float radiusScale;
uniform int tubeSlices;

const float maxLen = 0.1; // heuristic to exclude wrapped points

/// Emit a vertex of decreasing brightness to the end of the histrory line.
void AppendVertexToLine(vec3 start, vec3 end, float brightness)
{
    gl_Position = prmtx * mvmtx * vec4(start,1);
    g_f_DiffuseCol = vec4(vec3(brightness), 1);
    EmitVertex();
    
    gl_Position = prmtx * mvmtx * vec4(end,1);
    g_f_DiffuseCol = vec4(vec3(brightness), 1);
    EmitVertex();
}

/// Emit a set of triangles in a cylindrical shape around the history path.
void AppendTube(vec3 start, vec3 end, vec3 nextEnd, float radius, int slices)
{
    const float thresh = 0.01;

    vec3 axis = normalize(end - start);
    vec3 up   = normalize(vec3(0,1,0));
    if (dot(axis, up) > (1.0-thresh))
    {
        up = vec3(1,0,0);
    }
    vec3 left = cross(up, axis);
    up = cross(axis, left);

    float radscale = radiusScale * radius;
    up   *= radscale;
    left *= radscale;
    
    // Determine axis for the next segment to seal cylinder ends together
    vec3 axis2 = normalize(nextEnd - end);
    vec3 up2   = normalize(vec3(0,1,0));
    if (dot(axis2, up2) > (1.0-thresh))
    {
        up2 = vec3(1,0,0);
    }
    vec3 left2 = cross(up2, axis2);
    up2 = cross(axis2, left2);
    
    // Smaller radius here
    radscale = radiusScale * (radius - recencyStep);
    up2   *= radscale;
    left2 *= radscale;


    for (int i=0; i<slices; ++i)
    {
        const float PI = 3.1415927;
        float angle = 2 * PI * (float(i)/float(slices));
        float angle2 = 2 * PI * (float(i+1)/float(slices));
        vec3 pt0 = start + sin(angle )*up + cos(angle )*left;
        vec3 pt1 = end   + sin(angle )*up2 + cos(angle )*left2;
        vec3 pt2 = start + sin(angle2)*up + cos(angle2)*left;
        vec3 pt3 = end   + sin(angle2)*up2 + cos(angle2)*left2;
        vec3 norm0 = normalize(cross(pt2-pt0,pt1-pt0));
        //vec3 norm1 = normalize(cross(pt3-pt2,pt1-pt2));
        ///@todo lighting in frag shader

        /// First tri
        gl_Position = prmtx * mvmtx * vec4(pt0,1);
        g_f_DiffuseCol = vec4(norm0, 1);
        EmitVertex();
        gl_Position = prmtx * mvmtx * vec4(pt1,1);
        g_f_DiffuseCol = vec4(norm0, 1);
        EmitVertex();
        gl_Position = prmtx * mvmtx * vec4(pt2,1);
        g_f_DiffuseCol = vec4(norm0, 1);
        EmitVertex();

        // Second tri
        gl_Position = prmtx * mvmtx * vec4(pt2,1);
        g_f_DiffuseCol = vec4(norm0, 1);
        EmitVertex();
        gl_Position = prmtx * mvmtx * vec4(pt1,1);
        g_f_DiffuseCol = vec4(norm0, 1);
        EmitVertex();
        gl_Position = prmtx * mvmtx * vec4(pt3,1);
        g_f_DiffuseCol = vec4(norm0, 1);
        EmitVertex();
    }

    EndPrimitive();
}

void main(void)
{
    vec3 curPt  = gl_in[0].gl_Position.xyz;
    vec3 lastPt = v_g_Pos1[0];
    
    /// Don't draw lines between history points that have wrapped
    if (length(curPt - lastPt) > maxLen)
        return;
    
#if USE_TRIANGLES
    AppendTube(curPt, lastPt, v_g_Pos2[0], recency, tubeSlices);
#else
    AppendVertexToLine(curPt, lastPt, recency);
#endif


    EndPrimitive();
}
]]

local hist_frag = [[
// vortex_history.frag
#version 330

in vec4 g_f_DiffuseCol;
out vec4 fragColor;

void main(void)
{
    fragColor = g_f_DiffuseCol;
}
]]



--[[
    Scatter instance positions randomly in particles array.
]]
local randomize_instance_positions_comp_src = [[
#version 430
#line 199

struct inst {
    vec4 pos;
    vec4 vel;
    vec4 str;
    vec4 dstr;
};

layout(local_size_x=256) in;
layout(std430, binding=0) buffer pblock { inst items[]; };
uniform int numPoints;

float hash( float n ) { return fract(sin(n)*43758.5453); }

#define PI 3.14159265359

void main()
{
    uint index = gl_GlobalInvocationID.x;
    if (index >= numPoints)
        return;
    float fi = float(index+1) / float(numPoints);

#if 0
    // random
    items[index].pos = vec4(hash(fi), hash(fi*3.), hash(fi*17.), .004);

    vec3 randv = vec3(hash(fi*19.), hash(fi*17.), hash(fi*7.));
    vec3 motiondir = (.1+2.*hash(fi*37.)) * normalize(2.*(vec3(-.5) + randv));
    motiondir = pow(length(motiondir), 7.) * normalize(motiondir);
    items[index].vel = vec4(0.);

    items[index].str = vec4(.05*motiondir, 0.);
#endif

    const vec3 center = vec3(.5);
    const vec3 normal = vec3(0., 1., 0.);
    const vec3 up = vec3(0., 0., 1.);
    const vec3 right = normalize(cross(normal, up));

    const float radSquared = .004;
    const float radius = .2;
    const vec3 sinPart = radius * sin(2.*PI*fi) * right;
    const vec3 cosPart = radius * cos(2.*PI*fi) * up;
    const vec3 vpos = center + sinPart + cosPart;
    items[index].pos = vec4(vpos, radSquared);
    items[index].vel = vec4(0.);

    const vec3 delta = normalize(cross(center-vpos, normal));
    items[index].str = vec4(delta, 0);

    items[index].dstr = vec4(0.);
}
]]
local function randomize_instance_positions()
    if progs.scatter_instances == nil then
        progs.scatter_instances = sf.make_shader_from_source({
            compsrc = randomize_instance_positions_comp_src,
            })
    end

    for r=1,ringSz do
        local pvbo = ringVbos[r]
        gl.BindBufferBase(GL.SHADER_STORAGE_BUFFER, 0, pvbo[0])
        local prog = progs.scatter_instances
        local function uni(name) return gl.GetUniformLocation(prog, name) end
        gl.UseProgram(prog)
        gl.Uniform1i(uni("numPoints"), numPoints)
        gl.DispatchCompute(numPoints/256+1, 1, 1)
        gl.UseProgram(0)
    end
end


local vortex_advection_comp_src = [[
// vortex_advection.comp
#version 430

///@todo #include
// Match the structs in VortexScene.h
struct vortex {
    vec4 position;
    vec4 velocity;
    vec4 strength;
    vec4 strengthDelta;
};

///@todo a Stopgap version until we use uniform blocks
struct VortexParams
{
    float timestep;        ///< simulation time step
    float radSquared;      ///< default initial particle radius-squared
    float gravity;         ///< magnitude of extra force preventing particles from going too high
                           ///<   only used for 332 and 112 vortex influence kernels
    float viscosity;       ///< scales time-based increase in particle *radius squared*
    float damping;         ///< scales time-based reduction in particle *strength*
    float defaultStrength; ///< default strength for all new particles
    float maxStrength;     ///< peak strength allowed for particles
    float periodicCopies;  ///< number of copies of periodic domains in each direction
    float freestreamX;     ///< global freestream velocity (not using a float3)
    float freestreamY;     ///< global freestream velocity
    float freestreamZ;     ///< global freestream velocity
};


layout(local_size_x=64, local_size_y=1, local_size_z=1) in;

///@todo Is it more efficient to write to a separate buffer?
layout(std430, binding=0) buffer pblockin { vortex vortsIn[]; };
layout(std430, binding=1) buffer pblockout { vortex vortsOut[]; };

VortexParams params;
void main()
{
    ///@todo Pass this in directly as a uniform block
    params.timestep    = .3 * 1.0e-5;
    params.damping     = 0.;
    params.maxStrength = 3.;


    int idx = int(gl_GlobalInvocationID);

    // These are written to the output buffers at the end of the kernel.
    vec4 p = vortsIn[idx].position;

    // Update positions using only the new velocity
    p += params.timestep * vortsIn[idx].velocity;


#if 0
    // bring particle in [0,1] bounds
    if (wrapXY) wrap_xy(p);
    if (wrapZ) {
        wrap_z(p);
    } else {
        // if the new position is under the ground plane, put it slightly above
        if (p.z < 0.f) p.z = 0.00001f;
    }
#endif

    // Now, do strengths
    vec4 s = vortsIn[idx].strength;
    s += params.timestep * vortsIn[idx].strengthDelta;

    // cap strength, lest it increase indefinitely
    const float maxStr = params.maxStrength;
    s.xyz *= clamp(maxStr/length(s.xyz), 0., 1.);

    // weaken the particles
    s *= 1.0f - clamp(params.damping * params.timestep, 0., 0.999);

    // weaken the source/sink strength 5x faster
    s.w *= 1.0f - clamp(5.0f * params.damping * params.timestep, 0., 0.999);

    // Write resulting values to output arrays.
    vortsOut[idx].position = p;
    vortsOut[idx].strength = s;

    // And copy the fluxes to the output arrays as well
    vortsOut[idx].velocity = vortsIn[idx].velocity;
    vortsOut[idx].strengthDelta = vortsIn[idx].strengthDelta;
}

]]

local vortex_motion_comp_src = [[
// vortex_motion.comp
#version 430

// Match the structs in VortexScene.h
struct vortex {
    vec4 position;
    vec4 velocity;
    vec4 strength;
    vec4 strengthDelta;
};

///@todo a Stopgap version until we use uniform blocks
struct VortexParams
{
    float timestep;        ///< simulation time step
    float radSquared;      ///< default initial particle radius-squared
    float gravity;         ///< magnitude of extra force preventing particles from going too high
                           ///<   only used for 332 and 112 vortex influence kernels
    float viscosity;       ///< scales time-based increase in particle *radius squared*
    float damping;         ///< scales time-based reduction in particle *strength*
    float defaultStrength; ///< default strength for all new particles
    float maxStrength;     ///< peak strength allowed for particles
    float periodicCopies;  ///< number of copies of periodic domains in each direction
    float freestreamX;     ///< global freestream velocity (not using a float3)
    float freestreamY;     ///< global freestream velocity
    float freestreamZ;     ///< global freestream velocity
};

layout(local_size_x=64, local_size_y=1, local_size_z=1) in;

layout(location = 0) uniform int u_count;

///@todo Is it more efficient to write to a separate buffer?
layout(std430, binding=0) buffer pblockin { vortex vortsIn[]; };
layout(std430, binding=1) buffer pblockout { vortex vortsOut[]; };



/// Inner vortex kernel
void addOneVortexInfluence0(
    float dx,
    float dy,
    float dz,
    vec4 srcStr,
    vec4 targStr,
    float radSquared,
    float factor,
    float mirrorFactor,
    inout vec4 inoutVel,
    inout vec4 inoutStrDel)
{
    float dist_squared = dx*dx + dy*dy + dz*dz;
    // Add the smoothness parameter
    dist_squared += radSquared;

    // Find reciprocal of cube of distance
    float dist = sqrt(dist_squared);
    float thisFactor = 1.0f / (dist * dist_squared);

    // Weaken the source particle's influence based on its distance from
    // the target particle (to allow for smooth boundary transitions)
    thisFactor *= factor * mirrorFactor;
    float thisStrengthZ = srcStr.z * mirrorFactor;

    // Compute cross product of strength of source particle j
    // and scaled distance vector, but z strength is modified by kk
    vec3 thisVel = vec3(0.f);
    thisVel.x = dz*srcStr.y - dy*thisStrengthZ;
    thisVel.y = dx*thisStrengthZ - dz*srcStr.x;
    thisVel.z = dy*srcStr.x - dx*srcStr.y;

    // Scale the results by that factor
    inoutVel.xyz += thisVel * thisFactor;


    // Use straight-up velocity gradient for calculating vortex stretching
    // Here, I am calculating the velocity gradient 3 terms at a time
    float bbb = -3. * thisFactor / (dist * dist);
    vec3 thisGrad = bbb * dx * thisVel;
    thisGrad.y += 2.f * factor * thisStrengthZ;
    thisGrad.z += -2.f * factor * srcStr.y;
    inoutStrDel.xyz += targStr.xyz * thisGrad;

    thisGrad = bbb * dy * thisVel;
    thisGrad.x += -2. * factor * thisStrengthZ;
    thisGrad.z += 2. * factor * srcStr.x;
    inoutStrDel.xyz += targStr.xyz * thisGrad;

    thisGrad = bbb * dz * thisVel;
    thisGrad.x += 2. * factor * srcStr.y;
    thisGrad.y += -2. * factor * srcStr.x;
    inoutStrDel.xyz += targStr.xyz * thisGrad;
}


/// Inner vortex kernel - velocity only - 27 FLOPs
void addOneVortexInfluence1(
    vec3 d,
    vec4 srcStr,
    float radSquared,
    float factor,
    float mirrorFactor,
    inout vec3 inoutVel)
{
    float dist_squared = d.x*d.x + d.y*d.y + d.z*d.z;
    // Add the smoothness parameter
    dist_squared += radSquared;

    // Find reciprocal of cube of distance
    float dist = sqrt(dist_squared);
    float thisFactor = 1.0f / (dist * dist_squared);

    // Weaken the source particle's influence based on its distance from
    // the target particle (to allow for smooth boundary transitions)
    thisFactor *= factor * mirrorFactor;

    // Compute cross product of strength of source particle j
    // and scaled distance vector, but z strength is modified by kk
    //vec3 delZ = vec3(d.x, d.y, d.z*mirrorFactor);
    //vec3 thisVel = cross(srcStr.xyz, delZ);

    vec3 thisVel = cross(srcStr.xyz, d);

    // Scale the results by that factor
    inoutVel += thisVel * thisFactor;
}

/// Inner vortex kernel - velocity only - 27 FLOPs
void addOneVortexSourceInfluence1(
    float dx,
    float dy,
    float dz,
    vec4 srcStr,
    float radSquared,
    float factor,
    float mirrorFactor,
    vec3 inoutVel)
{
    float dist_squared = dx*dx + dy*dy + dz*dz;
    // Add the smoothness parameter
    dist_squared += radSquared;

    // Find reciprocal of cube of distance
    float dist = sqrt(dist_squared);
    float thisFactor = 1.0 / (dist * dist_squared);

    // Weaken the source particle's influence based on its distance from
    // the target particle (to allow for smooth boundary transitions)
    thisFactor *= factor * mirrorFactor;
    float thisStrengthZ = srcStr.z * mirrorFactor;

    // Compute cross product of strength of source particle j
    // and scaled distance vector, but z strength is modified by kk
    vec3 thisVel = vec3(0.);
    thisVel.x = dz*srcStr.y      - dy*thisStrengthZ + dx*srcStr.w;
    thisVel.y = dx*thisStrengthZ - dz*srcStr.x      + dy*srcStr.w;
    thisVel.z = dy*srcStr.x      - dx*srcStr.y      + dz*srcStr.w;

    // Scale the results by that factor
    inoutVel += thisVel * thisFactor;
}

shared vortex vshared[gl_WorkGroupSize.x];

VortexParams params;
void main()
{
    int idx = int(gl_GlobalInvocationID);

    ///@todo Pass this in directly as a uniform block
    params.radSquared = .004;
    params.gravity = 30.;
    params.viscosity = 3.;
    params.freestreamX = 0.;
    params.freestreamY = 0.;
    params.freestreamZ = 0.;



    // Influence 112


    // Short names for referential convenience.
    // These are written to the output buffers at the end of the kernel.
    vec4 p = vortsIn[idx].position;

    // In order to compute the stretching term, we solve for
    // the velocity not on the particle position itself, but
    // on two ends of a very short stick centered at the
    // particle center. First, define the stick:
    vec3 stick = vortsIn[idx].strength.xyz;
    stick = normalize(stick);
    stick *= 0.1f * sqrt(vortsIn[idx].position.w);
    float stickLen = length(stick);
    vec3 pLeft = p.xyz - 0.5f*stick;
    vec3 pRight = p.xyz + 0.5f*stick;


    // Grab source vortexes and put them into shared memory

    /// Add in influence of each vortex: the idea here is to spin the leaf around the
    /// center of the vortex. The influence of the vortex should wane with distance.

    // velocity is the flux of position (+rad squared)
    vec3 velLeft = vec3(0.0);
    vec3 velRight = vec3(0.0);

#if 1
    // loop over batches of vortexes, each batch has blockDim.x vortexes
    for (int iblock=0; iblock<=(u_count-1)/gl_WorkGroupSize.x; iblock++) {

        // load in the vortexes
        vshared[gl_LocalInvocationID.x] = vortsIn[iblock*gl_WorkGroupSize.x+gl_LocalInvocationID.x];
        barrier();

        uint vortexBlockCount = gl_WorkGroupSize.x;
        // account for the final block potentially having fewer vortexes
        if (u_count < gl_WorkGroupSize.x * (iblock+1)) {
            vortexBlockCount = u_count - gl_WorkGroupSize.x * iblock;
        }

        // 1245 flops per i
        for (int i=0; i<vortexBlockCount; ++i)
        {
            vortex vj = vshared[i];

            // finally, loop over z-original and z-reflection
            // 121 flops/iter

            // Compute influence of this copy of the source vortex
            // onto the target leaf
            vec3 pos = vj.position.xyz;
            vec3 del = pLeft - pos;
            addOneVortexInfluence1(del, vj.strength, vj.position.w,
                                   1.0f, 1.0f, velLeft);

            del = pRight - pos;
            addOneVortexInfluence1(del, vj.strength, vj.position.w,
                                   1.0f, 1.0f, velRight);

            // and do the mirror of the source from beneath the ground
            pos.y *= -1.0f;
            del = pLeft - pos;
            addOneVortexInfluence1(del, vj.strength, vj.position.w,
                                   1.0f, -1.0f, velLeft);

            del = pRight - pos;
            addOneVortexInfluence1(del, vj.strength, vj.position.w,
                                   1.0f, -1.0f, velRight);

        } // end loop over vortexes

        barrier();

    } // end loop over blocks of vortexes
#else
    // Non-tiled version
    for (int i=0; i<u_count; ++i)
    {
        vortex v = vortsIn[i];

        for (int kk=-1; kk<2; kk+=2)
        {
            // Compute influence of this copy of the source vortex
            // onto the target leaf
            // 27 + 27 + 7 = 61 flops
            vec3 pos = v.position.xyz;
            pos.y *= float(kk);
            vec3 del = pLeft - pos;

            addOneVortexInfluence1(del, v.strength, v.position.w,
                                   1.0f, float(kk), velLeft);

            del = pRight - pos;

            addOneVortexInfluence1(del, v.strength, v.position.w,
                                   1.0f, float(kk), velRight);
        }
    }
#endif

    // Create and assemble the final velocity vector
    vec4 vel = vec4(0.0);

    // Apply global freestream
    vec3 freeStream = vec3(params.freestreamX, params.freestreamY, params.freestreamZ);
    vel.xyz = 0.5*(velLeft + velRight) + freeStream;

    // Add force to pull vortexes down if they get too high
    // Apply attractive force from a global central line xy=(0.5,0.5)
    const vec3 centerPull = vec3(0.5f, 0.5f, 0.05f);
    vel.xyz += params.gravity * (centerPull - p.xyz);

    // Viscous diffusion increases radius squared by a multiple of the viscosity
    //   this goes in to velocity.w
    vel.w = params.viscosity;

    // Write resulting values to output arrays.
    vortsOut[idx].velocity = vel;

    // multiply the target strength by the velocity gradient tensor to get
    //   the target strength flux
    vec3 stretch = (velRight - velLeft) * 1.f/stickLen;
    vortsOut[idx].strengthDelta = vec4(stretch.xyz, 0.0f);

    // Copy the positions from the input struct to the output struct, unmodified
    vortsOut[idx].position = vortsIn[idx].position;
    vortsOut[idx].strength = vortsIn[idx].strength;
}

]]

local function timestep_instances(dt)
    if progs.vort_motion == nil then
        progs.vort_motion = sf.make_shader_from_source({
            compsrc = vortex_motion_comp_src,
            })
    end
    if progs.vort_advection == nil then
        progs.vort_advection = sf.make_shader_from_source({
            compsrc = vortex_advection_comp_src,
            })
    end

    local nextPtr = ringPtr + 1
    if nextPtr > ringSz then nextPtr = 1 end
    local vboIn = ringVbos[ringPtr]
    local vboTemp = vbos.temp
    local vboOut = ringVbos[nextPtr]

    do
        local prog = progs.vort_motion
        local function uni(name) return gl.GetUniformLocation(prog, name) end

        gl.UseProgram(prog)
        gl.Uniform1i(0, numPoints)
        gl.BindBufferBase(GL.SHADER_STORAGE_BUFFER, 0, vboIn[0])
        gl.BindBufferBase(GL.SHADER_STORAGE_BUFFER, 1, vboTemp[0])
        gl.DispatchCompute(numPoints/16+1, 1, 1)
    end

    gl.MemoryBarrier(GL.SHADER_STORAGE_BARRIER_BIT)

    do
        local prog = progs.vort_advection
        local function uni(name) return gl.GetUniformLocation(prog, name) end

        gl.UseProgram(prog)
        gl.BindBufferBase(GL.SHADER_STORAGE_BUFFER, 0, vboTemp[0])
        gl.BindBufferBase(GL.SHADER_STORAGE_BUFFER, 1, vboOut[0])
        gl.DispatchCompute(numPoints/16+1, 1, 1)
    end

    gl.UseProgram(0)

    ringPtr = nextPtr
end

local function init_per_instance_attributes()
    local stride = 4 * 4 * ffi.sizeof('GLfloat') -- bytes
    local sz = stride * numPoints

    for i=1,ringSz do
        local ipvbo = glIntv(0)
        gl.GenBuffers(1, ipvbo)
        gl.BindBuffer(GL.ARRAY_BUFFER, ipvbo[0])
        gl.BufferData(GL.ARRAY_BUFFER, sz, nil, GL.STREAM_COPY)
        table.insert(ringVbos, ipvbo)
    end

    local ipvbo = glIntv(0)
    gl.GenBuffers(1, ipvbo)
    gl.BindBuffer(GL.ARRAY_BUFFER, ipvbo[0])
    gl.BufferData(GL.ARRAY_BUFFER, sz, nil, GL.STREAM_COPY)
    vbos.temp = ipvbo
end

local function init_programs()
    progs.hist = sf.make_shader_from_source({
        vsrc = hist_vert,
        gsrc = hist_geom,
        fsrc = hist_frag,
        })
end

function vortices_scene.initGL()
    init_programs()

    local vaoId = ffi.new("int[1]")
    gl.GenVertexArrays(1, vaoId)
    vao = vaoId[0]
    gl.BindVertexArray(vao)
    do
        init_per_instance_attributes()
        randomize_instance_positions()
    end
    gl.BindVertexArray(0)
end

function vortices_scene.exitGL()
    for _,p in pairs(progs) do
        gl.DeleteProgram(p)
    end
    progs = {}

    gl.BindVertexArray(vao)
    do
        for _,v in pairs(vbos) do
            gl.DeleteBuffers(1,v)
        end
        vbos = {}
        for _,v in pairs(ringVbos) do
            gl.DeleteBuffers(1,v)
        end
        ringVbos = {}
    end
    gl.BindVertexArray(0)
    local vaoId = ffi.new("GLuint[1]", vao)
    gl.DeleteVertexArrays(1, vaoId)
end

function vortices_scene.render_for_one_eye(view, proj)
    local prog = progs.hist
    local function uni(name) return gl.GetUniformLocation(prog, name) end
    gl.UseProgram(prog)
    gl.UniformMatrix4fv(uni("prmtx"), 1, GL.FALSE, glFloatv(16, proj))
    
    local m = {}
    for i=1,16 do m[i] = view[i] end
    local s = 1.4
    mm.glh_scale(m, s,s,s)
    mm.glh_translate(m, -.5, -1.95, -.25)

    gl.UniformMatrix4fv(uni("mvmtx"), 1, GL.FALSE, glFloatv(16, m))
    gl.Uniform1f(uni("radiusScale"), .01);
    gl.Uniform1i(uni("tubeSlices"), 6);

    gl.BindVertexArray(vao)
    local stride = 4*4*ffi.sizeof('GLfloat') -- bytes
    for i=0,histSz-1 do

        local ringIdx = ringPtr - i
        if ringIdx < 1 then ringIdx = ringIdx + ringSz end
        local lastIdx = ringPtr - i - 1
        if lastIdx < 1 then lastIdx = lastIdx + ringSz end
        local lastIdx2 = ringPtr - i - 2
        if lastIdx2 < 1 then lastIdx2 = lastIdx2 + ringSz end

        local vb0 = ringVbos[ringIdx]
        local vb1 = ringVbos[lastIdx]
        local vb2 = ringVbos[lastIdx2]

        gl.BindBuffer(GL.ARRAY_BUFFER, vb0[0])
        gl.VertexAttribPointer(0, 3, GL.FLOAT, GL.FALSE, stride, nil)
        gl.BindBuffer(GL.ARRAY_BUFFER, vb1[0])
        gl.VertexAttribPointer(1, 3, GL.FLOAT, GL.FALSE, stride, nil)
        gl.BindBuffer(GL.ARRAY_BUFFER, vb2[0])
        gl.VertexAttribPointer(2, 3, GL.FLOAT, GL.FALSE, stride, nil)

        gl.Uniform1f(uni("recency"), 1-i/histSz)
        gl.Uniform1f(uni("recencyStep"), 1/histSz)

        gl.EnableVertexAttribArray(0)
        gl.EnableVertexAttribArray(1)
        gl.EnableVertexAttribArray(2)

        gl.DrawArrays(GL.POINTS, 0, numPoints)
    end
    gl.BindVertexArray(0)
    gl.UseProgram(0)
end

function vortices_scene.timestep(absTime, dt)
    timestep_instances(dt+.000000001)
end

function vortices_scene.keypressed(ch)
    local reinit = false
    if ch == '-' then
        numPoints = numPoints / 2
        reinit = true
    elseif ch == '=' then
        numPoints = numPoints * 2
        reinit = true
    end

    if reinit then
        print(numPoints)
        vortices_scene.exitGL()
        vortices_scene.initGL()
    end
end

function vortices_scene.getNumPoints() return numPoints end

return vortices_scene
